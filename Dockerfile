FROM centos:7

RUN yum update -y &&  yum install -y epel-release \
    && yum install -y golang alsa-lib-devel \
    gcc rpm-build rpm-devel rpmlint make python \
    bash coreutils diffutils patch rpmdevtools \
    && useradd golang \
    && yum clean all \
    && rm -rf /var/cache/yum

USER golang
RUN go get -u golang.org/x/lint/golint
ENV GOPATH "/home/golang/go/bin"
ENV PATH "$PATH:$GOPATH"
ENTRYPOINT ["go"]
CMD ["version"]
